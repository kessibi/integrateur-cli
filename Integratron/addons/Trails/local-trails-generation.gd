extends "trails-generation.gd"

func _process(delta):
	if(!mesh_instance.mesh):
		return null
	var middle = (top_left + bottom_left)/2
	
	# Creation d'un nouveau plan uniquement lorque la moto a suffisament avancé
	
	if(middle.distance_to(node.global_transform.origin) >= separation):
		# Creation d'un nouveau plan
		var top_right    = Position.translation + node.global_transform.origin + node.global_transform.basis.y *.5*width
		var bottom_right = Position.translation + node.global_transform.origin - node.global_transform.basis.y *.5*width
		create_quad(top_right, bottom_right)
		
		
	
	#print(mesh_instance.mesh.surface_get_arrays(curLen - 1))