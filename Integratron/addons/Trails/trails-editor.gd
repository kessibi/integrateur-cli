tool
extends EditorPlugin

func get_name(): 
	return "Trails"

func _init():
	print("TRAILS INIT")

func _enter_tree():
	add_custom_type("Trail","Spatial",preload("trails-generation.gd"),preload("trails-icon.png"))
func _exit_tree():
	remove_custom_type("Trail")
