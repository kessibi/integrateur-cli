extends Spatial

var mesh_instance
var static_body_instance
var collision_shape_instance
var surface_tool

var Position

export (float) var width = 2.0

export (NodePath) var pathToNode
var node

var top_left = Vector3(0,0,0)
var bottom_left = Vector3(0,0,0)

export (int) var maxLen = 200
var curLen = 0

export (float) var separation = 3.0


export (Texture) var material


export (bool) var isColliding = false

func get_triangle_normal(a, b, c):
	# find the surface normal given 3 vertices
	var side1 = b - a
	var side2 = c - a
	var normal = side1.cross(side2)
	return normal.normalized()


func _ready():
	set_as_toplevel(true)
	set_translation(Vector3(0,0,0))
	set_rotation(Vector3(0,0,0))
	mesh_instance = MeshInstance.new()
	add_child(mesh_instance)
	
	static_body_instance = StaticBody.new()
	collision_shape_instance = CollisionShape.new()
	static_body_instance.add_child(collision_shape_instance)
	add_child(static_body_instance)
	
	mesh_instance.mesh = null
	mesh_instance.material_override = material
	surface_tool = SurfaceTool.new()
	
	node = get_node(pathToNode)
	
	Position = Position3D.new()
	#Position.translate(Vector3(0,0,0))
	
	top_left = Position.translation + node.global_transform.origin + node.global_transform.basis.y *.5*width
	bottom_left = Position.translation + node.global_transform.origin - node.global_transform.basis.y *.5*width
	
	#return 
	collision_shape_instance.shape = ConvexPolygonShape.new()
	collision_shape_instance.shape.points.insert(0,Vector3(-1,-1,-1))
	

###########################################
# Création des trainées effectué avec des "triangle strip"
#
# Ordre de création des vertex :
#
# - http://www.riemers.net/images/Tutorials/XNA/Csharp/Series3/strip1.jpg
# - http://ogldev.atspace.co.uk/www/tutorial27/triangle_strip2.jpg
#
#
# Le face culling est desactivé au niveau des shaders pour
# pouvoir voir les triangles des deux côtés.
#
#  # # # # # # #
#  # A SAVOIR  #
#  # # # # # # #
#  - la normale est bonne pour tous les points uniquement lorsqu'ils sont
#    sur le meme plan
#
#
###########################################

func interpolate_quad(top_right, bottom_right,diff):
	var top_AB = (top_right - top_left)/diff
	var bottom_AB = (bottom_right - bottom_left)/diff
	for i in range(0,diff):
		create_quad(top_left + top_AB, bottom_left + bottom_AB)
		pass
	pass

func create_quad(top_right, bottom_right):
	curLen += 1
	
	
	var normal = get_triangle_normal(top_left,bottom_right,top_right)
	
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	
	surface_tool.add_normal(normal)
	
	surface_tool.add_uv(Vector2(0,0))  
	surface_tool.add_vertex(bottom_left)
	
	
	surface_tool.add_uv(Vector2(0,1))  
	surface_tool.add_vertex(top_left)
	
	
	surface_tool.add_uv(Vector2(1,0))
	surface_tool.add_vertex(bottom_right)
	
	
	surface_tool.add_uv(Vector2(1,1))
	surface_tool.add_vertex(top_right)
	
	surface_tool.commit(mesh_instance.mesh)
	
	
	surface_tool.clear()
	
	
	if(isColliding):
		var a = PoolVector3Array([bottom_left,top_left,bottom_right])
		var b = collision_shape_instance.shape.points
		b.append_array(a)
		collision_shape_instance.shape.points = b
		#print(collision_shape_instance.shape.points)
	
	if(curLen > maxLen):
		delete_oldest_quad()
	
	
	bottom_left = bottom_right
	top_left = top_right
	

func delete_oldest_quad():
	curLen -= 1
	mesh_instance.mesh.surface_remove(0)
	
	if(isColliding):
		var a = collision_shape_instance.shape.points 
		a.remove(0)
		a.remove(0)
		a.remove(0)
		collision_shape_instance.shape.points = a

func clear():
	if mesh_instance != null :
		mesh_instance.mesh = null
		curLen = 0
	pass

func init():
	mesh_instance.mesh = Mesh.new()
	top_left = Position.translation + node.global_transform.origin + node.global_transform.basis.y *.5*width
	bottom_left = Position.translation + node.global_transform.origin - node.global_transform.basis.y *.5*width
	pass