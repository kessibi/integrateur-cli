extends AudioStreamPlayer3D

var speed_node
var min_speed
var max_speed

# Called when the node enters the scene tree for the first time.
func _ready():
	speed_node = get_parent().get_parent().get_parent()
	min_speed = speed_node.get("min_speed")
	max_speed = speed_node.get("max_speed")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	
	var ratio = speed_node.get("linear_velocity").length()
	ratio = range_lerp(ratio, min_speed, max_speed, 0.0, 1.0)
	set_pitch_scale(0.3*ratio + 1.0)
	
	pass
