extends MeshInstance

var time = 0;
onready var trail = get_node("./../../Trail");

func _ready():
	for i in range(0,7):
		self.set_surface_material(i,self.get_surface_material(i).duplicate())
	
	var matDuplicate = trail.material.duplicate()
	trail.material = matDuplicate
	trail.mesh_instance.material_override = matDuplicate
	
	# Definitopn des variables pour le shader
	var mat = self.get_surface_material(0)
	mat = trail.material
	mat.set_shader_param("border",true)
	mat.set_shader_param("wave_force",15.0)
	mat.set_shader_param("hardness",5.0)
	mat.set_shader_param("width",1.7)
	mat.set_shader_param("scaleModifier",1.0)
	mat.set_shader_param("speed",1.0)
	
	
	
	
	pass


#func _physics_process(delta):
#	time += delta
#	var color = Color((cos(3 + time * 3.0)+1.0)/2.0,(cos(time * 2.0) + 1.0)/2.0,(cos(1.0 + time * 5.0) + 1.0)/2.0)
#	setBikeColor(color)

func setBikeColor(color) :
	
	var mat = self.get_surface_material(0)
	mat.emission = color
	self.set_surface_material(0, mat)
		
	for i in range(1,7):
		mat = self.get_surface_material(i)
		mat.albedo_color = color
		mat.emission = color
		self.set_surface_material(i, mat)
		
	
	mat = trail.material
	mat.set_shader_param("color",Vector3(color.r,color.g,color.b))
	
	
