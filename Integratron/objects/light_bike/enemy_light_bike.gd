extends "light_bike.gd"

var received_linear_velocity = Vector3()

var gravity

func _ready():
	pass

#Quand un objet entre en collison
func on_body_entered(body):
	pass
	
#Quand un objet sors de collision
func on_body_exited(body):
	pass



func _process(delta):
	pass

func _physics_process(delta):
	pass

func _integrate_forces(state):
	linear_velocity = received_linear_velocity
	pass

func set_received_linear_velocity(linear_velocity_to_set):
	received_linear_velocity = linear_velocity_to_set
	pass
