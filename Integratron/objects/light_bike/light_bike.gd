extends RigidBody

const min_speed = 45
const max_speed = 150

export(Color) var color = Color(1.0,0.0,0.0)
export(bool) var randomColor = false


var c = [ 	Color( 1.0, 0.0, 0.0 ) , \
			Color( 0.0, 1.0, 0.0 ) , \
			Color( 1.0, 0.0, 1.0 ) , \
			Color( 1.0, 1.0, 0.0 ) , \
			Color( 0.0, 1.0, 1.0 ) , \
			Color( 1.0, 1.0, 1.0 ) , \
			Color( 1.0, 0.0, 0.5 ) , \
			Color( 0.0, 1.0, 0.5 ) , \
			Color( 0.5, 1.0, 0.0 ) , \
			Color( 0.0, 1.0, 0.5 ) , \
			Color( 0.0, 0.5, 1.0 ) , \
			Color( 0.5, 0.0, 1.0 )  ]


func _ready() :
	linear_velocity = transform.basis.z.normalized() * min_speed
	if !randomColor :
		$BodyAndCam/BodyShape.setBikeColor(color)
	else :
		
		randomize()
		$BodyAndCam/BodyShape.setBikeColor(c[randi() % c.size()])
	pass

func _enter_tree():
	spawn(Vector3())
	pass

func _process(state) :
	if linear_velocity.length_squared() > 1.05 * max_speed*max_speed :
		linear_velocity /= linear_velocity.length()/max_speed
	if linear_velocity.length_squared() < 0.95 * min_speed*min_speed :
		linear_velocity /= linear_velocity.length()/min_speed
	pass
	
func spawn(location=Vector3(-50,350,0)):
	$Trail.clear()
	linear_velocity /= linear_velocity.length()/min_speed
	pass
