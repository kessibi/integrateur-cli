extends "light_bike.gd"

#Exports
export (float) var linear_acceleration = 30.0
export (float) var lean_speed          = 3.0
export (float) var air_control         = 0.3
export (float) var deadzone            = 0.8
export (float) var max_mobile_angle    = 6

export (NodePath) var camera_front     = NodePath("BodyAndCam/Camera/InterpolatedCamera")
export (NodePath) var camera_back      = NodePath("BodyAndCam/CameraBack/InterpolatedCamera")

#Control
var current_control = 1.0
var ground_air = 1.0
#Physics
var on_ground = 0
const max_lean = .25*PI
var rot = .0
var lean = .0
var lean_target = .0
var full_control = true

var input



var gravity

func mobile_input(delta):
	
	var lean_delta = lean_speed * delta
	
	var angle = Input.get_accelerometer().x;
	
	#VIRAGE
	#Calcul
	if abs(angle) > deadzone :
		lean_target = angle/max_mobile_angle * max_lean
	else :
		lean_target = 0.0
	
	#Application
	if abs(lean_target - lean) < 2 * lean_delta :
		lean = lean_target
	elif lean_target > lean :
		lean += lean_delta
	elif lean_target < lean :
		lean -= lean_delta
	else :
		if abs(lean) > 1.1 * lean_delta :
			if lean > 0 :
				lean -= lean_delta
			else :
				lean += lean_delta
		else :
			lean = 0
	pass

func _input(event) :
	if event is InputEventScreenTouch :
		
		var width_tres = get_viewport().size.x / 3;
		var position   = event.position.x;
		var action
		#A droite ou a gauche ?
		if position > width_tres * 2 :
			#On touche ou on lache ?
			if event.pressed :
				Input.action_press("accelerate")
			else :
				Input.action_release("accelerate")
		elif position < width_tres :
			
			#On touche ou on lache ?
			if event.pressed :
				Input.action_press("brake")
			else :
				Input.action_release("brake")
		

func keyboard_input(delta) :
	
	var lean_delta = lean_speed * delta
	
	#Turn
	if Input.is_action_pressed("turn_left") :
		lean -= lean_delta
		pass
	elif Input.is_action_pressed("turn_right") :
		lean += lean_delta
		pass
	
	#Redressement
	else :
		if abs(lean) > 1.1 * lean_delta :
			if lean > 0 :
				lean -= lean_delta
			else :
				lean += lean_delta
		else :
			lean = 0
	pass

func _on_body_entered_spawn(body):
	if body.get_collision_layer_bit(1) :
		$Trail.init()
		### FIN TEMPORAIRE
		$BodyAndCam/Camera.rotation.x = -PI/10
		$BodyAndCam/Camera.translation.z = 0
		$BodyAndCam/Camera.translation.y = 12
		disconnect("body_entered", self, "_on_body_entered_spawn")
		full_control = false
		
#		on_ground = 0;
		
	pass

func _enter_tree():
	#Déterminer gravité de la zone
	gravity = PhysicsServer.area_get_param(get_world().space,PhysicsServer.AREA_PARAM_GRAVITY_VECTOR) \
				* PhysicsServer.area_get_param(get_world().space,PhysicsServer.AREA_PARAM_GRAVITY)
	print("gravity : ", gravity)
	print(range(0,10))
	var cam = get_node(camera_front)
	cam.make_current()
	global.localbike = self
	spawn(Vector3(-30,350,0))
	$AnimationPlayer.play("land")
	connect("body_entered", self, "on_body_entered")
	connect("body_exited" , self, "on_body_exited")
	if OS.get_name() == "Android" :
		input = funcref(self, "mobile_input")
	else :
		input = funcref(self, "keyboard_input")
	$ViewportContainer.queue_free()
	$MeshInstance.queue_free()
	pass

func _physics_process(delta):
	if(full_control):
		return
	var ray_ground = get_world().direct_space_state.intersect_ray(self.translation, self.translation + Vector3(0,-1.5,0), [], 2) \
				  or get_world().direct_space_state.intersect_ray(self.translation +Vector3(0,0,4), self.translation + Vector3(0,-1.5,4), [], 2)
	
	if(ray_ground and not on_ground):
		$AnimationPlayer.play("land")
		print("land")
		on_ground = true
	elif(not ray_ground and on_ground):
		$AnimationPlayer.play("take_off")
		print("take off")
		on_ground = false
	return

func _process(delta):
	
	current_control = 1.0 + ground_air * (air_control - 1.0)

	input.call_func(delta)

	#Limite inclinaison
	var current_max_lean = current_control * max_lean
	lean = clamp(lean, -current_max_lean, current_max_lean)
	

	$BodyAndCam.rotation.z = lean
	$BodyAndCam/Camera.rotation.z = 0.75 * lean

	if Input.is_action_just_pressed("switchCamera"):
		print("back")
		var cam = get_node(camera_back)
		cam.make_current()
	if Input.is_action_just_released("switchCamera"):
		print("front")
		var cam = get_node(camera_front)
		cam.make_current()
	
	current_control = 1.0 + ground_air * (air_control - 1.0)
	
	# Vecteur direction avant (tiens compte de l'inclinaison du modèle)
	var forward = $BodyAndCam.transform.basis.z.normalized()
	# Vecteur direction haut (ne tiens pas compte de l'inclinaison du modèle)
	var upward = transform.basis.y.normalized()
	
	var linear_delta = linear_acceleration * delta
	

	#Gravité
	if $Timer.is_stopped() :
		linear_velocity += 6 * gravity*delta
	#Controles (au sol)
	if on_ground:
		pass
	#Controles (en l'air)
	else :
		linear_delta *= current_control
	pass
	
	linear_delta *= forward
	
	var current_speed = linear_velocity.length()
	
	#Application acceleration/freinage
	if Input.is_action_pressed("accelerate"):
		if  current_speed < max_speed - linear_delta.length():
			linear_velocity += linear_delta
		else :
			linear_velocity += (max_speed-current_speed)/current_speed * linear_delta.normalized() 
			
			pass
	if Input.is_action_pressed("brake"):
		if  current_speed > min_speed + linear_delta.length():
			linear_velocity -= linear_delta
		else :
			linear_velocity -= (current_speed - min_speed)/current_speed * linear_delta.normalized() 
		pass
	
	#Calcul des rotations de trajectoire
	var radius = .0
	if lean != 0:
		rot = -gravity.length() * 15 * tan(lean) / linear_velocity.length()
	else :
		rot = 0
		pass
	#Inclinaison du modèle

		pass
	
	if Input.is_action_pressed('respawn') :
		spawn(Vector3(0,500,0))
		pass

	
	#Application des rotations
	$BodyAndCam.rotate_y(rot * delta)
	linear_velocity = forward * Vector3(linear_velocity.x,0,linear_velocity.z).length() \
					  + Vector3(0, linear_velocity.y, 0)

	
	pass

func spawn(location=Vector3(-50,350,0)):
	print("spawn")
#	on_ground = 2
	#FIN TEMPORAIRE
	$Timer.set_one_shot(true)
	$Timer.set_wait_time(5)
	$Timer.start()
	$BodyAndCam/Camera.rotation.x = -PI/2
	$BodyAndCam/Camera.translation.y = 20;
	$BodyAndCam/Camera.translation.z = 5;
	set_translation(location)
	connect("body_entered", self, "_on_body_entered_spawn", [])
	$AnimationPlayer.play("land")
	on_ground = false
	full_control = true
	.spawn(location)

