extends HSlider


func _ready():
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	if not config.has_section_key("window", "shrink"):
		if OS.get_name() == "Android":
			config.set_value("window", "shrink", 2)
		else:
			config.set_value("window", "shrink", 1)
		config.save("user://settings.cfg")
	if err == OK:
		value = max_value - config.get_value("window", "shrink") + 1

func _on_ShrinkSlider_value_changed(value):
	global.shrink = value
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	
	if err == OK:
		config.set_value("window", "shrink", 1 + max_value - int(value))
		config.save("user://settings.cfg")
