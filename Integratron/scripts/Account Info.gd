extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var label
var helmet
var bike_color
var exit

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	helmet = get_node("Avatar")
	label = get_node("Label")
	
	var scale = 1 / ProjectSettings.get("display/window/stretch/shrink")
	
	
	#helmet.set_modulate(global.bike_color)
	label.set_text(global.username)
	label.set_anchors_preset(4)
	if(label.get_size().x<120):
		label.set_size(Vector2(120,label.get_size().y))
	else:
		set_size(Vector2(label.get_global_rect().size.x+100,label.get_size().y))
	
	label.set_anchors_preset(4)
	#label.set_position(Vector2(label.get_position().x-10,label.get_position().y))
	set_anchors_preset(PRESET_TOP_RIGHT)
	var width = get_viewport_rect().size.x
	set_position(Vector2(width - (label.get_size().x*1.8*scale),35*scale))
	rect_scale = Vector2(scale,scale)
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	bike_color = get_parent().get_parent().get_node("Bike/BodyAndCam/BodyShape").get_surface_material(0).emission
	helmet.set_modulate(bike_color)
	pass
