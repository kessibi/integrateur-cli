extends AudioStreamPlayer2D

var config = ConfigFile.new()
var err = config.load("user://settings.cfg")

func _ready():
	if err == OK:
		var vol = config.get_value("music", "volume")
		set("volume_db", log(vol)*20)
	else:
		var config = ConfigFile.new()
		config.set_value("music", "volume", 1)
		if OS.get_name() == "Android":
			config.set_value("window", "shrink", 2)
		else:
			config.set_value("window", "shrink", 1)
		config.save("user://settings.cfg")