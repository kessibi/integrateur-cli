extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var soundfx

func _ready():
	var scale = 1/ProjectSettings.get("display/window/stretch/shrink")
	rect_scale = Vector2(scale,scale)
	set_position(get_position()*scale)
	# Called when the node is added to the scene for the first time.
	# Initialization here
	soundfx = get_node("/root/MusicPlayer/SoundFX/ButtonSound")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_BackButtonLobby_pressed():
	soundfx.play()
	if get_tree().change_scene("res://scenes/home.tscn") != 0:
		OS.quit()
	pass # replace with function body
