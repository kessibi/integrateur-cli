extends Node

func _enter_tree():
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	
	if err == OK:
		if not config.has_section_key("window", "shrink"):
			if OS.get_name() == "Android":
				config.set_value("window", "shrink", 2)
			else:
				config.set_value("window", "shrink", 1)
			config.save("user://settings.cfg")
		global.shrink = config.get_value("window", "shrink") + 0.0
	
	$SettingsPanel.show()
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,Vector2(get_viewport().size.x,get_viewport().size.y)/global.shrink,1)
	ProjectSettings.set("display/window/stretch/shrink",global.shrink)
	pass

func _exit_tree():
	get_tree().set_screen_stretch(SceneTree.STRETCH_MODE_VIEWPORT,SceneTree.STRETCH_ASPECT_KEEP_HEIGHT,Vector2(get_viewport().size.x,get_viewport().size.y)*global.shrink,1)
	ProjectSettings.set("display/window/stretch/shrink",1)
	pass