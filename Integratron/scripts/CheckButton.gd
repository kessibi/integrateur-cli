extends CheckButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var sounds

# Called when the node enters the scene tree for the first time.
func _ready():
	sounds = get_node("/root/MusicPlayer/SoundFX").get_children()
	for i in sounds:
		if(i.volume_db==-20):
			set_pressed(true)
		else:
			set_pressed(false)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _on_CheckButton_pressed():
	for i in sounds:
		if(i.volume_db==-20):
			i.volume_db = -80
		else:
			i.volume_db = -20
	pass # Replace with function body.
