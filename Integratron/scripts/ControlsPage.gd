extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var mobile
var computer

# Called when the node enters the scene tree for the first time.
func _ready():
	mobile = get_node("Mobile")
	computer = get_node("Computer")
	if(OS.get_name() == "Android"):
		computer.hide()
		mobile.show()
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
