extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var soundfx

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	soundfx = get_node("/root/MusicPlayer/SoundFX/ButtonSound")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_CreateAccountButton_pressed():
	soundfx.play()
	if get_tree().change_scene("res://scenes/CreateAccount.tscn") != 0:
		get_tree().quit()
	pass # replace with function body
