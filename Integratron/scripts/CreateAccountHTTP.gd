extends Control

var username
var password1
var password2
var form
var message
var dataToSend
var pass_error
var acc_error
var error_sound
var validation_sound

func _ready():
	validation_sound = get_node("/root/MusicPlayer/SoundFX/Validation")
	error_sound = get_node("/root/MusicPlayer/SoundFX/ErrorSound")
	form = get_parent().get_parent().get_node("FormContainer")
	message = get_parent().get_parent().get_node("AccountCreated")
	pass_error = get_parent().get_node("FormInfo/ErrorPasswords")
	acc_error = get_parent().get_node("FormInfo/ErrorAccount")
	acc_error.hide()
	pass_error.hide()
	message.hide()
	pass

# Function called when the request is completed
func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	print("Result: "+str(result))
	if result == HTTPRequest.RESULT_SUCCESS:
		if response_code == 200:
			var data = JSON.parse(body.get_string_from_utf8())
			global.token = data.result.Token
			form.hide()
			message.show()
			global.username = username
			validation_sound.play()
			yield(get_tree().create_timer(2.0), "timeout")
			get_tree().change_scene("res://scenes/Lobby.tscn")
		elif response_code == 500:
			acc_error.show()
			error_sound.play()
			print("Account already created with this information")
		else:
			print("HTTP Error :"+str(response_code))

# Function that makes the request to create the account
func _make_post_request(url, data_to_send, use_ssl):
	# Convert data to json string:
	var query = JSON.print(data_to_send)
	# Add 'Content-Type' header:
	var headers = ["Content-Type: application/x-www-form-urlencoded","Content-Length: "+str(query.length())]
	return $HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)

# Function called when the button "create" is pressed
# A request to create the account is made.
func _on_CreateButton_pressed():
	username = get_parent().get_node("FormInfo/AccountUsername/AccountUsernameInput").get_text()
	password1 = get_parent().get_node("FormInfo/AcountPassword/AccountPasswordInput").get_text()
	password2 = get_parent().get_node("FormInfo/AccountPassword2/AccountPasswordInput2").get_text()
	if(password1==password2):
		dataToSend = {"username":username,"password":password1}
		var code = _make_post_request("https://integratron.paul-heng.fr/register",dataToSend,true)
		if(code != 0):
			print("An error occured! Error code: " + str(code))
	else:
		pass_error.show()
		error_sound.play()
		print("Passwords not identical! Try again!")
	pass # replace with function body

# Function called when the key "enter" is pressed
# A request to create the account is made.
func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			username = get_parent().get_node("FormInfo/AccountUsername/AccountUsernameInput").get_text()
			password1 = get_parent().get_node("FormInfo/AcountPassword/AccountPasswordInput").get_text()
			password2 = get_parent().get_node("FormInfo/AccountPassword2/AccountPasswordInput2").get_text()
			if(password1==password2):
				dataToSend = {"username":username,"password":password1}
				var code = _make_post_request("https://integratron.paul-heng.fr/register",dataToSend,true)
				if(code != 0):
					print("An error occured! Error code: " + str(code))
			else:
				pass_error.show()
				print("Passwords not identical! Try again!")
	pass