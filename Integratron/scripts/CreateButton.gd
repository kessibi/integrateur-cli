extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var username
var password1
var password2
var form
var message
var dataToSend
onready var ht = get_parent()

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	username = ""
	password1 = ""
	password2 = ""
	form = get_parent().get_parent().get_parent().get_node("FormContainer")
	message = get_parent().get_parent().get_parent().get_node("AccountCreated")
	message.hide()
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_CreateButton_pressed():
	username = get_parent().get_parent().get_node("FormInfo/AccountUsername/AccountUsernameInput").get_text()
	password1 = get_parent().get_parent().get_node("FormInfo/AcountPassword/AccountPasswordInput").get_text()
	password2 = get_parent().get_parent().get_node("FormInfo/AccountPassword2/AccountPasswordInput2").get_text()
	if(password1==password2):
		dataToSend = {"name":username, "password":password1}
		var code = ht._make_post_request("https://jsonplaceholder.typicode.com/todos/1",dataToSend,true)
		print(code)
		form.hide()
		message.show()
		yield(get_tree().create_timer(3.0), "timeout")
		get_tree().change_scene("res://scenes/Lobby.tscn")
	pass # replace with function body
