extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"


func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_EnterGameButton_pressed():
	global.server_adress = get_parent().get_node("IP").get_text()
	global.port = get_parent().get_node("Port").get_text()
	print("IP: " + global.server_adress)
	print("Port: " + global.port)
	print("Username=" + global.username)
	get_tree().change_scene("res://scenes/Main.tscn")
	pass # replace with function body
	
func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			global.server_adress = get_parent().get_node("IP").get_text()
			global.port = get_parent().get_node("Port").get_text()
			print("IP: " + global.server_adress)
			print("Port: " + global.port)
			print("Username: " + global.username)
			get_tree().change_scene("res://scenes/Main.tscn")
	pass
