extends Container

var i;
var time;
var idx = 0; 


func _ready():
	var scale = 1/ProjectSettings.get("display/window/stretch/shrink")
	
	rect_scale = Vector2(scale,scale)
	set_position(get_position() * scale)
	# Called when the node is added to the scene for the first time.
	# Initialization here
	global.texts = get_tree().get_nodes_in_group("eventTexts");
	for txt in global.texts:
		txt.set_use_bbcode(true);
	pass

func _process(delta):
	i = 0;
	while i < global.times.size():
		if(global.times[i] > 0):
			global.times[i] -= delta;
		i += 1;
	reorder()
	pass

func msg(UUID1, UUID2, type):
	time = OS.get_time();
	var h = str(time.hour) if time.hour > 10 else '0' + str(time.hour)
	var m = str(time.minute) if time.minute > 10 else '0' + str(time.minute)
	var s = str(time.second) if time.second > 10 else '0' + str(time.second)
	if type == 0:
		add_notif(h, m, s, UUID1, UUID2);
	else:
		playerEv(h, m, s, UUID1, UUID2)

func playerEv(h, m, s, uuid, event):
	var b = false;
	global.texts = get_tree().get_nodes_in_group("eventTexts");
	for txt in global.texts:
		if txt.get_text() == '':
			b = true;
			idx = global.texts.find(txt);
			txt.set_bbcode(h + ':' + m + ':' + s + ' [color=#ADD8E6][b]' + uuid +'[/b][/color] ' + event);
			global.times[idx] = 5;
			break;
	
	if !b:
		if(!global.texts.empty()) :
			global.texts[0].clear();
		reorder();
		global.texts[3].set_bbcode(h + ':' + m + ':' + s + ' [color=#ADD8E6][b]' + uuid +'[/b][/color] ' + event);
		global.times[3] = 5;

func add_notif(h, m, s, uuid1, uuid2):
	var b = false;
	global.texts = get_tree().get_nodes_in_group("eventTexts");
	for txt in global.texts:
		if txt.get_text() == '':
			b = true;
			idx = global.texts.find(txt);
			if uuid2 != "" && uuid1 != uuid2:
				txt.set_bbcode(h + ':' + m + ':' + s + ' [color=#ADD8E6][b]' + uuid1 +'[/b][/color] was defeated by [color=#ADD8E6][b]' + uuid2 + '[/b][/color]');
			else:
				txt.set_bbcode(h + ':' + m + ':' + s + ' [color=#ADD8E6][b]' + uuid1 +'[/b][/color] brutally killed himself.');
			global.times[idx] = 5;
			break;
	
	if !b:
		if(!global.texts.empty()) :
			global.texts[0].clear();
		reorder();
		if uuid2 != "" || uuid1 != uuid2:
			global.texts[3].set_bbcode(h + ':' + m + ':' + s + ' [color=#ADD8E6][b]' + uuid1 +'[/b][/color] was defeated by [color=#ADD8E6][b]' + uuid2 + '[/b][/color]');
		else:
			global.texts[3].set_bbcode(h + ':' + m + ':' + s + ' [color=#ADD8E6][b]' + uuid1 +'[/b][/color] brutally killed himself.');
			
		global.times[3] = 5;
	

func reorder():
	i = 0;
	var txt = '';
	while i < global.texts.size()-1:
		if global.texts[i].get_text() == '':
			txt = global.texts[i+1].get_bbcode();
			global.texts[i].set_bbcode(txt);
			global.texts[i+1].clear();
			global.times[i] = global.times[i+1];
			global.times[i+1] = 0;
		i += 1;
	pass
	