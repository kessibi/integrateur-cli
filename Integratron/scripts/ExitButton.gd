extends TextureButton

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var soundfx

# Called when the node enters the scene tree for the first time.
func _ready():
	soundfx = get_node("/root/MusicPlayer/SoundFX/ButtonSound")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_ExitButton_pressed():
	var title = get_parent().get_parent().get_parent().get_node("Title")
	var login = get_parent().get_parent().get_parent().get_node("LogInPanel")
	login.hide()
	soundfx.play()
	title.show()
	pass # Replace with function body.
