extends Control

var dataCorrect
var title
var loginpanel
var dataToSend
var username
var password
var error_msg
var error_sound
var validation_sound

func _ready():
	var scale = 1/ProjectSettings.get("display/window/stretch/shrink")
	set_position(get_position()*scale)
	# Called when the node is added to the scene for the first time.
	# Initialization here
	error_sound = get_node("/root/MusicPlayer/SoundFX/ErrorSound")
	validation_sound = get_node("/root/MusicPlayer/SoundFX/Validation")
	title = get_parent().get_parent().get_node("Title")
	loginpanel = get_parent().get_parent().get_node("LogInPanel")
	error_msg = get_parent().get_node("MainContainer/InputsContainer/WrongUP")
	error_msg.hide()
	dataCorrect = true
	pass

# Function called when the request is completed
func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	if result == HTTPRequest.RESULT_SUCCESS:
		if response_code == 200: #if connection successfull
			var data = JSON.parse(body.get_string_from_utf8())
			global.token = data.result.Token
			title.hide()
			loginpanel.hide()
			global.username = username
			validation_sound.play()
			if get_tree().change_scene("res://scenes/Lobby.tscn") != 0:
				get_tree().quit()
		elif response_code == 401: #if connection not successfull(aka. no account)
			error_msg.show()
			error_sound.play()
			print(str(response_code) + ": no account associated with this data.")
		else:
			print("HTTP Error :"+str(response_code))
			
	else:
		error_msg.set_text("ERROR: "+str(result))
		error_msg.show()
	pass # replace with function body

# Function that makes the request to connect to the account
func _make_post_request(url, data_to_send, use_ssl):
	# Convert data to json string:
	var query = JSON.print(data_to_send)
	#print(query)
	# Add 'Content-Type' header:
	var headers = ["Content-Type: application/json","Content-Length: "+str(query.length())]
	return $HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)

# Function called when the button "enter" on the log-in panel is pressed
# A request to connect to the account is made
func _on_EnterLogin_pressed():
	username  = get_node("InputsContainer/Username").get_text()
	password  = get_node("InputsContainer/Password").get_text()
	dataToSend = {"username": username, "password": password}
	var code = _make_post_request("https://integratron.paul-heng.fr/login", dataToSend, true)
	if(code != 0):
		print("An error occured! Error code: " + str(code))
	pass # replace with function body

# Function called when the key "enter" is pressed when you're on the log in panel
# A request to connect to the account is made
func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			username  = get_node("InputsContainer/Username").get_text()
			password  = get_node("InputsContainer/Password").get_text()
			dataToSend = {"username": username, "password": password}
			var code = _make_post_request("https://integratron.paul-heng.fr/login", dataToSend, true)
			if(code != 0):
				print("An error occured! Error code: " + str(code))
	pass