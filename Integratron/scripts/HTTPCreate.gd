extends Container

func _ready():
	pass


func _on_CreateButton_pressed():
	$HTTPRequest.request("https://jsonplaceholder.typicode.com/todos/1")
	


func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	var json = JSON.parse(body.get_string_from_utf8())
	print(json.result)
	print(response_code)
	
	

func _make_post_request(url, data_to_send, use_ssl):
	# Convert data to json string:
	var query = JSON.print(data_to_send)
	print(query)
	# Add 'Content-Type' header:
	var headers = ["Content-Type: application/json"]
	var code = $HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_POST, query)
	return code
	

#func connect():
	#var HTTP = HTTPClient.new()
	#var url = "http://10.40.11.166/"
	#var RESPONSE = HTTP.connect("10.40.11.166",80)