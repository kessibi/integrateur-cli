extends Control

# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var computer
var mobile

# Called when the node enters the scene tree for the first time.
func _ready():
	mobile = get_node("Controls/Mobile")
	computer = get_node("Controls/Computer")
	if(OS.get_name() == "Android"):
		computer.hide()
		mobile.show()
	yield(get_tree().create_timer(3.0), "timeout")
	get_tree().change_scene("res://scenes/Map1.tscn")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
