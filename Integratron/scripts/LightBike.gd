extends KinematicBody

export var speed = 20
var direction = Vector3()
var velocity = Vector3()


func _ready():
	
	pass

func _process(delta):
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass
	
func _physics_process(delta):
	
	direction = Vector3(0,0,0)
	
	if(Input.is_action_pressed("ui_left")):
		direction.x -= 1
		
	if(Input.is_action_pressed("ui_right")):
		direction.x += 1
		
	if(Input.is_action_pressed("ui_up")):
		direction.z -= 1
		
	if(Input.is_action_pressed("ui_down")):
		direction.z += 1
		
	direction = direction.normalized()
	#move_and_slide(direction * speed, Vector3(0,1,0))
	move_and_collide(direction * speed * delta)
	pass
