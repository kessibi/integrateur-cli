extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

#	ProjectSettings.set_setting("display/window/stretch/shrink", 1)
# Called when the node enters the scene tree for the first time.
func _ready():
	
	var scale = 1/ProjectSettings.get("display/window/stretch/shrink")
	rect_scale = Vector2(scale,scale)
	var position = get_position()
	position.y = position.y - scale
	set_position(position)