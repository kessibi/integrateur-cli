extends MeshInstance


func _ready():
	
	# Definition des variables pour le shader
	var mat = self.get_surface_material(0).duplicate()
	mat.set_shader_param("groundSize",10)
	self.set_surface_material(0,mat)

func _physics_process(delta):
	if(global.localbike != null):
		var position = Vector3(global.localbike.global_transform.origin.x,global.localbike.global_transform.origin.y,global.localbike.global_transform.origin.z)
		self.get_surface_material(0).set_shader_param("position",position)
	pass
