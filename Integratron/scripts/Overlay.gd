extends Container

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var shrink = ProjectSettings.get("display/window/stretch/shrink")
	rect_scale = Vector2(1/shrink, 1/shrink)
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
