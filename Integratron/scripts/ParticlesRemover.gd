extends Node
var time = 0;

func _ready():
	pass

func _process(delta):
	if(time == 75):
		var explos = get_tree().get_nodes_in_group("explosions");
		for expl in explos:
			expl.queue_free(); 
		time = 0;
	time += 1;