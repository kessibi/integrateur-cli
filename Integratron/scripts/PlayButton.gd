extends Button

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	if global.playername != "":
		get_parent().get_node("PseudoInput").set_text(global.playername)
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_PlayButton_pressed():
	global.playername = get_parent().get_node("PseudoInput").get_text()
	get_tree().change_scene("res://scenes/Lobby.tscn")
	pass # replace with function body

func _unhandled_input(event):
	if event is InputEventKey:
		if event.pressed and event.scancode == KEY_ENTER:
			global.playername = get_parent().get_node("PseudoInput").get_text()
			get_tree().change_scene("res://scenes/Lobby.tscn")
	pass