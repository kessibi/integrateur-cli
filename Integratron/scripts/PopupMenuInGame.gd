extends Panel

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var i;
var j;
var scoreboard
var settings

func _ready():
	var scale = 1 / ProjectSettings.get("display/window/stretch/shrink")
	rect_scale = Vector2(scale, scale)
	rect_position = (get_viewport().size - rect_size * scale) / 2 	# Called when the node is added to the scene for the first time.
	# Initialization here
	i=0
	j=0
	scoreboard = get_parent().get_node("PopupMenuInGame")
	settings = get_parent().get_node("SettingsPanel")
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass

func _unhandled_input(event):
	if OS.get_name() != "Android":
		if event is InputEventKey:
			if event.pressed and event.scancode == KEY_TAB:
				scoreboard.set_visible(!scoreboard.is_visible())
			if event.pressed and event.scancode == KEY_ESCAPE:
				settings.set_visible(!settings.is_visible())
	else:
		if event is InputEventScreenDrag:
			if event.relative.length_squared() > 100:
				if abs(event.relative.y) > abs(event.relative.x):
					if event.relative.y > 0 :
						settings.show()
					else :
						settings.hide()
				else :
					if event.relative.x > 0 : 
						scoreboard.show()
					else :
						scoreboard.hide()
	pass
