extends ProgressBar

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var speed_node
var min_speed
var max_speed
var label
var motorsound

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	motorsound = get_node("/root/MusicPlayer/SoundFX").get_children()[3]
	#set_loop(true)
	speed_node = get_parent().get_parent().get_parent().get_node("Bike")
	label = get_parent().get_node("Label")
	min_speed = speed_node.get("min_speed")
	max_speed = speed_node.get("max_speed")
	min_value = min_speed
	max_value = max_speed
	value = speed_node.get("linear_velocity").length()
	#print("VALUE: "+str(value))
	motorsound.play()
	pass

func _process(delta):
	value = speed_node.get("linear_velocity").length()
	label.set_text(str(value))
	#print(1+((value-45)*0.01))
	#motorsound .set_pitch_scale(1+((value-45)*0.01))
	# Called every frame. Delta is time since last frame.
	# Update game logic here.
	pass
