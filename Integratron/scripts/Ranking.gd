extends ScrollContainer

# class member variables go here, for example:
# var a = 2
# var b = "textvar"
var players
var container
var server_get_players
var data
var dataToSend
var players_names
var cpt = 0

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	container = get_node("Container")
	dataToSend = {"ID": global.server_hash}
	var rankings = _make_post_request("https://integratron.paul-heng.fr/servrankings", dataToSend, true)
	if(rankings != 0):
		print("An error occured! Error code: " + str(rankings))
	pass

# Updates the ranking every 200 frames
func _process(delta):
	 #Called every frame. Delta is time since last frame.
	 #Update game logic here.
	cpt += 1
	if(cpt%100==0):
		dataToSend = {"ID": global.server_hash}
		var rankings = _make_post_request("https://integratron.paul-heng.fr/servrankings", dataToSend, true)
		if(rankings != 0):
			print("An error occured! Error code: " + str(rankings))
		cpt = 0
	pass

# Adds all players in the raking (aka. the ranking scroll container)
func add_all_players_to_ranking(players):
	var my_index = find_me(players)
	add_player_to_ranking(players[my_index].DeathCount, players[my_index].KD, players[my_index].KillCount, players[my_index].Username, my_index)
	for i in range(players.size()):
		if(i!=my_index):
			add_player_to_ranking(players[i].DeathCount, players[i].KD, players[i].KillCount, players[i].Username, i)
	var control_node = Control.new()
	get_node("Container").add_child(control_node)

func find_me(players):
	for i in range(players.size()):
		if(players[i].Username == global.username):
			return i

func add_player_to_ranking(death, ratio, kills, username, pos):
	var control_node = Control.new()
	control_node.name = username
	var panel_node = Panel.new()
	panel_node.set_size(Vector2(400,30))
	panel_node.set_anchors_preset(PRESET_WIDE)
	panel_node.set("custom_styles/panel",load("res://themes/ranking_line.tres"))
	if(username==global.username):
		panel_node.set("custom_styles/panel",load("res://themes/ranking_line_green.tres"))
	var label1 = Label.new()
	var label2 = Label.new()
	var label3 = Label.new()
	var label4 = Label.new()
	var label5 = Label.new()
	control_node.add_child(panel_node)
	panel_node.add_child(label1)
	panel_node.add_child(label2)
	panel_node.add_child(label3)
	panel_node.add_child(label4)
	panel_node.add_child(label5)
	label1.set_anchors_preset(PRESET_TOP_LEFT)
	label1.set_size(Vector2(55,30))
	label1.set_text(str(pos+1))
	label1.align = HALIGN_CENTER
	label1.valign = VALIGN_CENTER
	label1.set("custom_fonts/font",load("res://themes/ranking_font.tres"))
	label2.set_anchors_preset(PRESET_TOP_LEFT)
	label2.set_size(Vector2(180,30))
	label2.set_position(Vector2(55,0))
	label2.set_text(username)
	label2.set("custom_fonts/font",load("res://themes/ranking_font.tres"))
	label2.align = HALIGN_CENTER
	label2.valign = VALIGN_CENTER
	label3.set_anchors_preset(PRESET_TOP_LEFT)
	label3.set_size(Vector2(55,30))
	label3.set_position(Vector2(235,0))
	label3.set_text(str(kills))
	label3.set("custom_fonts/font",load("res://themes/ranking_font.tres"))
	label3.align = HALIGN_CENTER
	label3.valign = VALIGN_CENTER
	label4.set_anchors_preset(PRESET_TOP_LEFT)
	label4.set_size(Vector2(55,30))
	label4.set_position(Vector2(290,0))
	label4.set_text(str(death))
	label4.set("custom_fonts/font",load("res://themes/ranking_font.tres"))
	label4.align = HALIGN_CENTER
	label4.valign = VALIGN_CENTER
	label5.set_anchors_preset(PRESET_TOP_LEFT)
	label5.set_size(Vector2(55,30))
	label5.set_position(Vector2(345,0))
	label5.set_text(str(stepify(ratio,0.01)))
	label5.set("custom_fonts/font",load("res://themes/ranking_font.tres"))
	label5.align = HALIGN_CENTER
	label5.valign = VALIGN_CENTER
	get_node("Container").add_child(control_node)
	pass

func remove_all_players():
	for i in range(container.get_child_count()):
		container.get_child(i).queue_free()

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	print("RESULT: "+str(result))
	if result == HTTPRequest.RESULT_SUCCESS:
		if response_code == 200: #if connection successfull
			data = JSON.parse(body.get_string_from_utf8())
			players_names = []
			remove_all_players()
			sort(data.result)
			add_all_players_to_ranking(data.result)
		elif response_code == 500: #if connection not successfull(aka. no account)
			print(str(response_code) + " : could not retrieve ranking!")
		else:
			print("HTTP Error :"+str(response_code))
	pass # replace with function body

func _make_post_request(url, data_to_send, use_ssl):
	var query = JSON.print(data_to_send)
	return $HTTPRequest.request(url,[], use_ssl, HTTPClient.METHOD_POST, query)

class MyCustomSorter:
	static func sort(a,b):
		if(a.KD > b.KD):
			return true
		return false

func sort(tab):
	tab.sort_custom(MyCustomSorter,"sort")