extends Control

var data
var buttons

func _ready():
	var scale = 1/ProjectSettings.get("display/window/stretch/shrink")
	rect_scale = Vector2(scale,scale)
	set_position(get_position()*scale)
	# Called when the node is added to the scene for the first time.
	# Initialization here
	get_node("NoServersMessage").hide()
	var s = get_node("ServersList")
	#s.hide()
	buttons = s.get_child_count()
	for i in range(buttons):
		get_node("ServersList/Button"+str(i)).hide()
	var servers = _make_get_request("https://integratron.paul-heng.fr/servers", true)
	if(servers != 0):
		print("An error occured! Error code: " + str(servers))
	pass

func _on_HTTPRequest_request_completed(result, response_code, headers, body):
	print("RESULT: "+str(result))
	if result == HTTPRequest.RESULT_SUCCESS:
		if response_code == 200: #if connection successfull
			data = JSON.parse(body.get_string_from_utf8())
			print(data.result)
			processing(data.result)
			get_node("ServersList").show()
		else:
			get_node("NoServersMessage").show()
			print("HTTP Error: "+str(response_code))
	pass # replace with function body

func _make_get_request(url, use_ssl):
	# Convert data to json string:
	var headers = ["Content-Type: application/json","Authorization: Bearer "+str(global.token)]
	return $HTTPRequest.request(url, headers, use_ssl, HTTPClient.METHOD_GET)

func _on_Button_pressed():
	var servers = _make_get_request("https://integratron.paul-heng.fr/servers", true)
	if(servers != 0):
		print("An error occured! Error code: " + str(servers))
	pass # replace with function body

func button_pressed0():
	global.server_hash = data.result[0].Hash
	global.server_adress = data.result[0].IP
	global.port = int(data.result[0].Port)
	get_tree().change_scene("res://scenes/InfoBeforeGame.tscn")

func button_pressed1():
	global.server_hash = data.result[1].Hash
	global.server_adress = data.result[1].IP
	global.port = int(data.result[1].Port)
	get_tree().change_scene("res://scenes/InfoBeforeGame.tscn")

func button_pressed2():
	global.server_hash = data.result[2].Hash
	global.server_adress = data.result[2].IP
	global.port = int(data.result[2].Port)
	get_tree().change_scene("res://scenes/InfoBeforeGame.tscn")
	
func processing(data):
	get_node("NoServersMessage").hide()
	var size = data.size()
	var button
	if(len(data)==0):
		#show message
		get_node("NoServersMessage").show()
	for i in range(size):
		button = get_parent().get_node("Servers/ServersList/Button"+str(i))
		if(!button.is_visible()):
			button.show()
		button.text = data[i].Name + " | Players: " + str(data[i].Players)
		if(!button.is_connected("pressed",self,"button_pressed"+str(i))):
			button.connect("pressed", self, "button_pressed"+str(i))
	pass
