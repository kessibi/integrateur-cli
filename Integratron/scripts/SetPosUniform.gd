extends MeshInstance

func _ready():
	self.get_surface_material(0).set_shader_param("groundSize",global_transform.basis.get_scale().x)

func _physics_process(delta):
	if(global.localbike != null):
		self.get_surface_material(0).set_shader_param("position",global.localbike.global_transform.origin)
#	print(localBike.global_transform.origin)
	pass
