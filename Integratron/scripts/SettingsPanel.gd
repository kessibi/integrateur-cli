extends Panel

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

# Called when the node enters the scene tree for the first time.
func _ready():
	var scale = 1/ProjectSettings.get("display/window/stretch/shrink")
	
	rect_scale = Vector2(scale,scale)
	set_anchors_preset(Control.PRESET_CENTER_TOP)
	set_position(Vector2((get_viewport().size.x - rect_size.x*scale)/2 ,-5))
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
