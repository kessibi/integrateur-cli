extends HSlider

# class member variables go here, for example:
# var a = 2
# var b = "textvar"

func _ready():
	# Called when the node is added to the scene for the first time.
	# Initialization here
	var node = get_node("/root/MusicPlayer/Music/AudioStreamPlayer2D")
	var volumeValue = node.get_volume_db()
	value = pow(2.718,volumeValue/20)
	print("VALUE: "+str(value))
	pass

#func _process(delta):
#	# Called every frame. Delta is time since last frame.
#	# Update game logic here.
#	pass


func _on_ProgressBar_value_changed(value):

	var node = get_node("/root/MusicPlayer/Music/AudioStreamPlayer2D")

	node.set_volume_db(log(value)*20)
	
	var config = ConfigFile.new()
	var err = config.load("user://settings.cfg")
	
	if err == OK:
		config.set_value("music", "volume", value)
		config.save("user://settings.cfg")
