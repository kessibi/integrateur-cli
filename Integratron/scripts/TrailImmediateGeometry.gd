#extends Spatial
extends ImmediateGeometry

var PositionUp
var PositionDown
onready var lightBike

var lastV3up = Vector3(0,0,0)
var lastV3down = Vector3(0,0,0)

func _ready():
	PositionUp = get_node("./Up")
	PositionDown = get_node("./Down")
	lastV3up = PositionUp.translation
	lastV3down = PositionDown.translation
	
	
	lightBike = get_node("../LightBike")

func get_triangle_normal(a, b, c):
    # find the surface normal given 3 vertices
    var side1 = b - a
    var side2 = c - a
    var normal = side1.cross(side2)
    return normal.normalized()

	
func _process(delta):
#func _physics_process(delta):
	
	#Vec3
	var v3up = PositionUp.translation + lightBike.global_transform.origin
	#Vec3
	var v3down = PositionDown.translation + lightBike.global_transform.origin
	
	var middle = lastV3up + lastV3down
	middle /= 2
	
	if(middle.distance_to(lightBike.global_transform.origin) >= 3.0):
		
		#begin(Mesh.PRIMITIVE_LINES,null)
		begin(Mesh.PRIMITIVE_TRIANGLE_STRIP,null)
		
		
		
		
		###########################################
		#
		# Ordre de création des vertex
		#
		# http://www.riemers.net/images/Tutorials/XNA/Csharp/Series3/strip1.jpg
		#
		# http://ogldev.atspace.co.uk/www/tutorial27/triangle_strip2.jpg
		#
		#
		# Le face culling est desactivé au niveau des shaders pour
		# pouvoir voir les triangles des deux côtés.
		###########################################
		
		set_normal(get_triangle_normal(lastV3up,v3down,v3up))
		set_uv(Vector2(0,0))
		add_vertex(lastV3down)
		set_uv(Vector2(0,1))
		add_vertex(lastV3up)
		set_uv(Vector2(1,0))
		add_vertex(v3down)
		set_uv(Vector2(1,1))
		add_vertex(v3up)
		
		end()
		
		lastV3down = v3down
		lastV3up = v3up

