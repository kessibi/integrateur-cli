extends Spatial

var mesh_instance
var collision_shape
var surface_tool

var PositionUp
var PositionDown

export (NodePath) var pathToLightBike
var light_bike

var top_left = Vector3(0,0,0)
var bottom_left = Vector3(0,0,0)

var top_right = Vector3(0,0,0)
var bottom_right = Vector3(0,0,0)

export (int) var maxLen = 1000
var curLen = 0


func get_triangle_normal(a, b, c):
	# find the surface normal given 3 vertices
	var side1 = b - a
	var side2 = c - a
	var normal = side1.cross(side2)
	return normal.normalized()


func _ready():
	mesh_instance = get_node("MeshInstance")
	mesh_instance.mesh = Mesh.new()
	collision_shape = get_node("StaticBody/CollisionShape")
	surface_tool = SurfaceTool.new()
	
	PositionUp = get_node("./Up")
	PositionDown = get_node("./Down")
	top_left = PositionUp.translation
	bottom_left = PositionDown.translation
	
	light_bike = get_node(pathToLightBike)


###########################################
# Création des trainées effectué avec des "triangle strip"
#
# Ordre de création des vertex :
#
# - http://www.riemers.net/images/Tutorials/XNA/Csharp/Series3/strip1.jpg
# - http://ogldev.atspace.co.uk/www/tutorial27/triangle_strip2.jpg
#
#
# Le face culling est desactivé au niveau des shaders pour
# pouvoir voir les triangles des deux côtés.
#
#
#  # # # # # # #
#  # A SAVOIR  #
#  # # # # # # #
#  - la normale n'est bonne pour tous les points uniquement lorsqu'ils sont
#    sur le meme plan
#
#
###########################################

func create_quad():
	curLen += 1
	
	top_right    = PositionUp.translation + light_bike.global_transform.origin
	bottom_right = PositionDown.translation + light_bike.global_transform.origin
	
	
	var normal = get_triangle_normal(top_left,bottom_right,top_right)
	
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLE_STRIP)
	
	surface_tool.add_normal(normal)
	
	surface_tool.add_uv(Vector2(0,0))  
	surface_tool.add_vertex(bottom_left)
	
	
	surface_tool.add_uv(Vector2(0,1))  
	surface_tool.add_vertex(top_left)
	
	
	surface_tool.add_uv(Vector2(1,0))
	surface_tool.add_vertex(bottom_right)
	
	
	surface_tool.add_uv(Vector2(1,1))
	surface_tool.add_vertex(top_right)
	
	surface_tool.commit(mesh_instance.mesh)
	
	surface_tool.clear()
	
	
	# La collsion n'est pas ajouté
	#
	# -> collision_shape.shape = surface_tool.commit(mesh_instance.mesh).create_convex_shape()
	
	bottom_left = bottom_right
	top_left = top_right
	

func delete_oldest_quad():
	curLen -= 1
	mesh_instance.mesh.surface_remove(0)

func _process(delta):

	var middle = (top_left + bottom_left)/2
	print("eee");
	# Creation d'un nouveau plan uniquement lorque la moto a suffisament avancé
	
	if(middle.distance_to(light_bike.global_transform.origin) >= 1.0):
		# Creation d'un nouveau plan
		create_quad()
		
		if(curLen > maxLen):
			delete_oldest_quad()

	#print(mesh_instance.mesh.surface_get_arrays(curLen-1))
	
