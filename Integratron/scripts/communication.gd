extends Node
var socketUDP = PacketPeerUDP.new()
const proto_b = preload("res://protobuf/barrier.gd")
const proto = preload("res://protobuf/clientToserv.gd")
const proto2 = preload("res://protobuf/servToclient.gd")
onready var Bike = preload("res://objects/light_bike/light_bike.tscn")
onready var LocalBike = preload("res://objects/light_bike/local_light_bike.tscn")
onready var Explosion = preload("res://objects/Explosion.tscn")
var c;
var msg; var snd;
var seq = 0; var seq_barrier = 0;
var array_bytes;
var serv; var bike;
var result_code;
var tab; var bottom = Vector3(0, 0, 0); var top = Vector3(0, 0, 0);
var last_seq = 0;
var x; var y; var z; var p; var speed; var l;
var playerName = global.username;
var chat;
var timer;

var trail;

enum PlayerState{NULL=0, DEAD=1, ALIVE=2}

#Tableaux associatifs des joueurs, states : mort/vivant seq : numéro de barrière
var player_states
var player_seq

func _notification(what):
	if what == MainLoop.NOTIFICATION_WM_QUIT_REQUEST or what == MainLoop.NOTIFICATION_WM_GO_BACK_REQUEST \
	or (OS.get_name() == "Android" and what == MainLoop.NOTIFICATION_WM_UNFOCUS_REQUEST):
		get_tree().change_scene("res://scenes/Lobby.tscn")

func initMsgBarrier(seqNum):
	var father = global.localbike
	var trail = father.get_node("Trail");
	if(!trail.mesh_instance.mesh):
		return null
	if(bottom != trail.bottom_left && top != trail.top_left):

		seq_barrier = (seq_barrier + 1) % global.MaxSeq	
		var msg = proto_b.barrier.new()
		var upperPoint = msg.new_upperPoint()
		var lowerPoint = msg.new_lowerPoint()
		
		bottom = trail.bottom_left;
		top = trail.top_left;
	
		upperPoint.set_i(top.x)
		upperPoint.set_j(top.y)
		upperPoint.set_k(top.z)
	
		lowerPoint.set_i(bottom.x)
		lowerPoint.set_j(bottom.y)
		lowerPoint.set_k(bottom.z)
	
		msg.set_sequenceNumber(seqNum);
		msg.set_UUID(playerName);
		return msg;
	
	return null;

func _ready():
	chat = get_parent().get_node("Overlay").get_node("EventContainer")
	socketUDP.set_dest_address(global.server_adress, global.port)
	socketUDP.listen(5000, "0.0.0.0", 65536)
	player_states = Dictionary()
	player_seq = Dictionary()
	
	pass

func _send_infos():
	if(socketUDP.is_listening()):
		msg = initMsg(seq);
		seq = (seq+1) % global.MaxSeq;
		snd = PoolByteArray();
		snd.append_array('m'.to_ascii());
		snd.append_array(msg.to_bytes());
		socketUDP.put_packet(snd);
	
	pass

func _process(delta):
	
	msg = initMsgBarrier(seq_barrier)
	if(msg != null and socketUDP.is_listening()):
		snd = PoolByteArray();
		snd.append_array('b'.to_ascii());
		snd.append_array(msg.to_bytes());
		socketUDP.put_packet(snd);
		
	#Si on a un message
	if socketUDP.get_available_packet_count():
		
		array_bytes = socketUDP.get_packet()
		serv = proto2.servToClient.new();
		result_code = serv.from_bytes(array_bytes);
		array_bytes.resize(0);
		#Si pas d'erreur à la désérialization
		if result_code == proto2.PB_ERR.NO_ERRORS:
			if (serv.get_sequenceNumber() > last_seq || serv.get_sequenceNumber() < global.MaxSeq / 2):
				tab = serv.get_enemyState();
				seq = (seq+1) % global.MaxSeq;
				#Pour chaque UUID déjà enregistré
				for uuid in player_states.keys():
					if !(uuid in tab.keys()):
						player_states.erase(uuid)
						player_seq.erase(uuid)
						var bike = get_parent().get_node(uuid)
						bike.queue_free()
						
				#Pour chaque UUID contenu dans le message
				for uuid in tab.keys():
					if !(uuid in player_states.keys()) :
						player_states[uuid] = PlayerState.DEAD
						player_seq[uuid] = 0
					#Si c'est le joueur local on le choisit simplement pour la suite
					if uuid == playerName :
						bike = global.localbike;
						
					#Si c'est un ennemi on mets à jour sa position, et ses barrières
					else:
						bike = get_parent().get_node(uuid)
						updateBike(uuid, tab[uuid].get_mat(), tab[uuid].get_state());
						get_parent().get_node(uuid).get_node("ViewportContainer/Viewport/BikeName").set_text(uuid)
							
							
					#Si le joueur a changé d'état
					if(tab[uuid].get_state() != player_states[uuid]):
						player_states[uuid] = tab[uuid].get_state();
						#S'il est mort
						if player_states[uuid] == PlayerState.DEAD:
							#Affichage
							chat.msg(uuid, tab[uuid].get_killer(), 0)
							#BOUM
							var newExplosion = Explosion.instance();
							newExplosion.transform = bike.transform;
							newExplosion.add_to_group("explosions");
							get_parent().add_child(newExplosion);
							#Spawn
							bike.spawn(Vector3(-30,350,0))
						if player_states[uuid] == PlayerState.ALIVE:
							#Affichage
							if bike != global.localbike :
								bike.get_node("Trail").init()
							chat.msg(uuid, "is back in the field", 1)
							#Initialisation trainée
		while socketUDP.get_available_packet_count():
			socketUDP.get_packet()
	pass


func initMsg(seqNum):
	var node = global.localbike;
	var msg = proto.clientToServ.new();
	var position = msg.new_mp();
	var body = node.get_node('BodyAndCam');
	var transform = body.get_global_transform();
	
	# transform de BodyAndCam
	x = position.new_x();
	x.set_i(transform[0][0]);
	x.set_j(transform[0][1]);
	x.set_k(transform[0][2]);
	
	y = position.new_y();
	y.set_i(transform[1][0]);
	y.set_j(transform[1][1]);
	y.set_k(transform[1][2]);
	
	z = position.new_z();
	z.set_i(transform[2][0]);
	z.set_j(transform[2][1]);
	z.set_k(transform[2][2]);
	
	p = position.new_p();
	p.set_i(transform[3][0]);
	p.set_j(transform[3][1]);
	p.set_k(transform[3][2]);
	
	l = node.get_linear_velocity();
	speed = position.new_speed();
	speed.set_i(l[0]);
	speed.set_j(l[1]);
	speed.set_k(l[2]);
	

	msg.set_sequenceNumber(seqNum);
	msg.set_UUID(playerName);
	return msg;
	

func updateBike(UUID, position, state):
	var node = get_parent().get_node(UUID);
	if(node == null):
		var newBike = Bike.instance();
		newBike.set_name(UUID);
		get_parent().add_child(newBike, true);
		bike = newBike;
	
	var body = bike.get_node('BodyAndCam');

	if(position != null):
		
		bike.transform[0][0] = position.get_x().get_i();
		bike.transform[0][1] = position.get_x().get_j();
		bike.transform[0][2] = position.get_x().get_k();
		
		bike.transform[1][0] = position.get_y().get_i();
		bike.transform[1][1] = position.get_y().get_j();
		bike.transform[1][2] = position.get_y().get_k();
	
		bike.transform[2][0] = position.get_z().get_i();
		bike.transform[2][1] = position.get_z().get_j();
		bike.transform[2][2] = position.get_z().get_k();
		
		bike.transform[3][0] = position.get_p().get_i();
		bike.transform[3][1] = position.get_p().get_j();
		bike.transform[3][2] = position.get_p().get_k();
	
		l = position.get_speed();
		bike.set_linear_velocity(Vector3(l.get_i(), l.get_j(), l.get_k()));
		
func addBarrier(barrier,diff):
	var u = Vector3(barrier.get_upperPoint().get_i(),\
					barrier.get_upperPoint().get_j(),\
					barrier.get_upperPoint().get_k());
	var l = Vector3(barrier.get_lowerPoint().get_i(),\
					barrier.get_lowerPoint().get_j(),\
					barrier.get_lowerPoint().get_k());
	bike.get_node("Trail").interpolate_quad(u,l,diff);
	