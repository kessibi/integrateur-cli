shader_type spatial;

uniform sampler2D noiseImage;
uniform vec3 color = vec3(1.0,1.0,1.0);
render_mode unshaded,depth_draw_alpha_prepass, blend_add;

void vertex()
{
	MODELVIEW_MATRIX = INV_CAMERA_MATRIX * mat4(CAMERA_MATRIX[0],CAMERA_MATRIX[1],CAMERA_MATRIX[2],WORLD_MATRIX[3]);
}


mat2 makem2(in float theta){float c = cos(theta);float s = sin(theta);return mat2(vec2(c,-s),vec2(s,c));}

float noise( in vec2 x ){return texture(noiseImage, x*0.01).x;}

float fbm(in vec2 p)
{	
	float z=2.0;
	float rz = 0.0;
	vec2 bp = p;
	for (float i= 1.0;i < 6.0;i++)
	{
		rz+= abs((noise(p)-0.5)*2.0)/z;
		z = z*2.0;
		p = p*2.0;
	}
	return rz;
}

float dualfbm(in vec2 p, float time)
{
    //get two rotated fbm calls and displace the domain
	vec2 p2 = p*0.7;
	vec2 basis = vec2(fbm(p2-time*1.6),fbm(p2+time*1.7));
	basis = (basis-.5)*.2;
	p += basis;
	
	//coloring
	return fbm(p*makem2(time*0.2));
}


void fragment()
{

	vec2 p = UV -0.5;
	
	float time = TIME * 0.3; 
	
    float rz = dualfbm(p,time);

	p /= exp(sin(time*10.0) * 3.1415);
	
	//final color
	vec3 col = color/rz;

    float d = 1.0 - distance(vec2(0.5),UV);
	
	ALBEDO = col / 15.0 * pow(d,9.0);
}


void light(){
	DIFFUSE_LIGHT = vec3(0.0,0.0,0.0);
	SPECULAR_LIGHT = vec3(0.0,0.0,0.0);
}