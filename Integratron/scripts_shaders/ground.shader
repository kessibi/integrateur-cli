shader_type spatial;
render_mode blend_add;

// Position de la moto
uniform vec3 position = vec3(100.0,10.0,-100.0);

// Taille/Scale du sol
uniform float groundSize = 1000.0;

// Nombre de cases de la grille
uniform float gridSize = 80.0;

// Couleur de la grille
uniform vec3 gridColor = vec3(0.8,0.8,0.8);

// Attenuation pour l'antialiasing
uniform float attenuation = 1.0;


void fragment()
{
	
	vec2 uv = UV * vec2(gridSize);
	
	// Creation de la grille
	float d = smoothstep(0.0,0.02,abs(mod(uv.y ,1.0) - 0.5));
	d *= smoothstep(0.0,0.02,abs(mod(uv.x ,1.0) - 0.5));
	d = 1.0-d;
	
	// Coloration de la grille
    vec3 rayColor = gridColor * d ;
	
	// Calcul de la valeur d'attenuation
	float anti = max(1.0 - distance( UV - vec2(0.5) , position.xz / sqrt(pow(groundSize * 2.0,2.0))), 0.0 );
	anti = pow(anti,attenuation);

	// Calcul de l'attenuation
	vec3 col = rayColor * anti;

	// Definition de la couleur
	ALBEDO = col;
	
}


void light(){
	DIFFUSE_LIGHT = ALBEDO;
	SPECULAR_LIGHT = ALBEDO;
}
	
