shader_type spatial;
render_mode cull_disabled;
uniform float wave_force = 10.0;

void vertex() {
	// Offset les vertex pour voir les trainées en ligne droite
    VERTEX.x += (sin(TIME + VERTEX.z + VERTEX.y)) / wave_force; 
    VERTEX.z += (cos(TIME + VERTEX.x + VERTEX.y)) / wave_force;
}

void fragment() {
	// Défini la couleur à la valeur de la normale
	ALBEDO = vec3(NORMAL.x,NORMAL.y,NORMAL.z);
}

