shader_type spatial;
render_mode cull_disabled;

uniform float wave_force = 15.0;

uniform float hardness = 5.0;
uniform float width = 1.7;
uniform float scaleModifier = 3.0;
uniform float speed = 1.0;
uniform bool border = false;
uniform float alphaMultiplicator = 1.0;

uniform vec3 color = vec3(0.2,0.9,0.2);
uniform sampler2D text : hint_black;

void vertex() {
	// Offset les vertex pour voir les trainées en ligne droite
    VERTEX.x += (sin(TIME * speed + VERTEX.z + VERTEX.y)) / wave_force; // offset vertex x by sine function on time elapsed
    VERTEX.z += (cos(TIME * speed + VERTEX.x + VERTEX.y)) / wave_force; // offset vertex x by sine function on time elapsed
}

void fragment() {
    vec3 f;
	// Tesr si les les bords sont activés
	if(border){
		
		// Crée les bord
		vec3 v = vec3(UV.x * scaleModifier);
	    v -= vec3(0.5);
	    v = abs(v);
	    v *= v;
		//v *= vec3(hardness);
		v *= v;
		v *= v;
		v *= v;
	     
	    vec3 h = vec3(UV.y * scaleModifier);
	    h -= vec3(0.5);
	    h = abs(h);
	    h *= h;
		h *= vec3(hardness);
		h *= h;
		h *= h;
		h *= h;
	    
	    f = v + h;
	}
    else
		f=vec3(0.0);
    
	
	
    vec2 uv2 = UV.xy;
 
    uv2.x = TIME/256.0;
    uv2.y /= 4.0;
	
	
	vec3 t = texture(text,uv2).rgb;
    
	f+=t;
	
	clamp(f,0.0,1.0);
	
    vec3 colFinal = f * color / 2.0;
	
    float colAlpha = (colFinal.x+colFinal.y+colFinal.z)/3.0;
	
	ALBEDO = colFinal;
	//ALBEDO = t;
	ALPHA = colAlpha * alphaMultiplicator;
	
	
	
}