shader_type spatial;
render_mode cull_disabled;

uniform float wave_force = 15.0;
uniform sampler2D text : hint_black;

void vertex() {
	// Offset les vertex pour voir les trainées en ligne droite
    VERTEX.x += (sin(TIME + VERTEX.z) * VERTEX.y) / wave_force;
    VERTEX.z += (sin(TIME + VERTEX.x) * VERTEX.y) / wave_force;
}

void fragment() {
	// Affiche la texture
	ALBEDO = texture(text,UV).rgb;
}
