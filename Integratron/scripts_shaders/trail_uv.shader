shader_type spatial;
render_mode cull_disabled;
uniform float wave_force = 15.0;

void vertex() {
	// Offset les vertex pour voir les trainées en ligne droite
    VERTEX.x += (sin(TIME + VERTEX.z) * VERTEX.y) / wave_force; 
    VERTEX.z += (sin(TIME + VERTEX.x) * VERTEX.y) / wave_force; 

}

void fragment() {
	// Affiche les uvs 
    ALBEDO = vec3(UV.x, UV.y, 0.0); 
}

