shader_type spatial;
render_mode cull_disabled;

uniform float wave_force = 15.0;

uniform float hardness = 5.0;
uniform float width = 1.7;
uniform float scaleModifier = 3.0;
uniform float speed = 1.0;
uniform bool border = true;
uniform float alphaMultiplicator = 1.0;

uniform vec3 color = vec3(0.2,0.9,0.2);

// Renvoie un voronoi
vec3 voronoi2D(vec2 uvs, float time){
    // q entre 0 et 1
	vec2 q = uvs;
       
    // Zoom 
    vec2 p = 7.0*q;
    
    vec2 pi = floor(p);    
    vec4 v = vec4( pi.xy, pi.xy + 1.0);
    v -= 64.0*floor(v*0.015);
    v.xz = v.xz*1.435 + 34.423;
    v.yw = v.yw*2.349 + 183.37;
    v = v.xzxz*v.yyww;
    v *= v;
    
    v *= time*0.00004 + 0.5;
	v *= speed;
    vec4 vx = 0.25*sin(fract(v*0.00047)*6.2831853);
    vec4 vy = 0.25*sin(fract(v*0.00074)*6.2831853);
    
    vec2 pf = p - pi;
    vx += vec4(0.0, 1.0, 0.0, 1.0) - vec4(pf.x);
    vy += vec4(0.0, 0.0, 1.0, 1.0) - vec4(pf.y);
    v = vx*vx + vy*vy;
    
    v.xy = min(v.xy, v.zw);
    return vec3(min(v.x,v.y));
}


void vertex() {
	// Offset les vertex pour voir les trainées en ligne droite
    VERTEX.x += (sin(TIME * speed + VERTEX.z + VERTEX.y)) / wave_force; // offset vertex x by sine function on time elapsed
    VERTEX.z += (cos(TIME * speed + VERTEX.x + VERTEX.y)) / wave_force; // offset vertex x by sine function on time elapsed
}

void fragment() {
    vec3 f;
	// Tesr si les les bords sont activés
	if(border){
		
		// Crée les bord
		vec3 v = vec3(UV.x * scaleModifier);
	    v -= vec3(0.5);
	    v = abs(v);
	    v *= v;
		//v *= vec3(hardness);
		v *= v;
		v *= v;
		v *= v;
	     
	    vec3 h = vec3(UV.y * scaleModifier);
	    h -= vec3(0.5);
	    h = abs(h);
	    h *= h;
		h *= vec3(hardness);
		h *= h;
		h *= h;
		h *= h;
	    
	    f = v + h;
	}
    else
		f=vec3(0.0);
    
	
    vec2 uv2 = UV.xy;
 
    uv2.y = TIME/256.0;
    
    
    vec3 t = voronoi2D(UV.xy * scaleModifier,TIME);

    f += t;
    
    vec3 colFinal = f * color;
	
    float colAlpha = (colFinal.x+colFinal.y+colFinal.z)/3.0;
	
	ALBEDO = colFinal;
	
	ALPHA = colAlpha * alphaMultiplicator;
	
}